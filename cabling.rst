Cabling
#######
* Description
* Repository: a WireViz source file can be placed in `this directory <https://gitlab.com/librespacefoundation/qubik/qubik-docs/-/tree/master/contrib?ref_type=heads>`__
* Releases

System Perfomance
*****************
.. warning:: Neither PVC bulk materials nor PVC plastic films shall be used in space applications, according to ECSS-Q-70-71A.

.. note:: Some wires like VBAT+ and VBAT- must be twisted. With the exception of the solar array, power lines shall be such that each line is twisted with its return, when the structure is not used as a return, as refered ECSS‐E‐ST‐20C.

System Assembly
***************

.. note:: Follow the `NASA workmanship guide <https://workmanship.nasa.gov/lib/insp/2%20books/frameset.html>`__ for cable lacing. Apply this in Main harness and COMMS Programmer cable. Use cyanoacrylate glue to solidify the knots in cable.

.. note:: Follow ECSS‐Q‐ST‐70‐08C for cabling assembly.

.. note:: The length of the wires mentioned below correspond to the length of the cable after the assembly. While cutting the wire, please add ~ 7mm of extra length.

* Color Abbreviations

  * RD -> Red
  * BK -> Black
  * BL -> Blue
  * YL -> Yellow

.. figure:: img/wire_length.png

   Wire cutting

.. figure:: img/cable-all.png
   :alt: cable-all

   QUBIK harnessing

.. figure:: img/Qubik-Cabling.png

   WireViz Illustration

Battery cable
==============


*  `DF11-6DS-2C <https://www.hirose.com/product/p/CL0543-0502-8-00>`__

   *  pin 1-2: VBAT+
   *  pin 3-4: VBAT-
   *  pin 5: COMMON (which is same with VBAT-)
   *  pin 6: TBAT
   *  `Connector assembly - DF11-2428SC <https://www.hirose.com/product/p/CL0550-0197-4-00>`__

.. figure:: img/battery-cable.png
   :alt: battery-cable

   Battery Cable

.. note:: As a convention we name (A) the battery attached on the BMPS board and (B) the other one.

.. note:: It is preferable to measure the colder battery, as cold is the more harsh condition for the battery performance.

.. note:: It is convenient to solder Common wire to (B) instead of (A).

.. list-table:: Battery Wiring
   :widths: 25 25 25 25 25
   :header-rows: 1

   * - Connector
     - Pin Name
     - Pin Connector
     - Pin Battery
     - Wire

   * - DF11-6DS-2C
     - VBAT+
     - 1 - 2
     - A(+) - B(+)
     - 26-AWG 45 mm (RD)


   * - DF11-6DS-2C
     - VBAT-
     - 3 - 4
     - A(-) - B (-)
     - 26-AWG 55 mm (BK)


   * - DF11-6DS-2C
     - Common
     - 5
     - A(C) or B(C)
     - 26-AWG 60 mm (BK)

   * - DF11-6DS-2C
     - TBAT
     - 6
     - A(T) or B(T)
     - 26-AWG 55 mm (YL)

.. figure:: img/battery-stack.png
   :alt: Battery-Stack

   Battery Stack

Antenna cable
=============

*  Length: 60mm
*  `U.FL-2LPHF6-066N1-A-60 <https://www.hirose.com/product/p/CL0321-5193-9-48>`__

.. figure:: img/antenna-cable.png
   :alt: antenna-cable

   Antenna Cable

Main harness
============

*  Connectors:

   *  `DF11-8DS-2C <https://www.hirose.com/en/product/p/CL0543-0503-0-00>`__

      *  pin 1: VBAT+
      *  pin 2: EPS-PWR
      *  pin 3: KILL-GND
      *  pin 4: GND
      *  pin 5: COMMS-GND (GND that no connected to in base plate GND)
      *  pin 6: ANT-REL
      *  pin 7: VBAT-SENSE
      *  pin 8: ANT-SENSE
      *  `Connector assembly - DF11-2428SC <https://www.hirose.com/product/p/CL0550-0197-4-00>`__

   *  `DF11-6DS-2C <https://www.hirose.com/product/p/CL0543-0502-8-00>`__

      *  pin 1: KILL-GND
      *  pin 2: GND
      *  pin 3: VBAT+
      *  pin 4: EPS-PWR
      *  pin 5: GND (TOP-PV-)
      *  pin 6: VBAT+ (TOP-PV+)
      *  `Connector assembly - DF11-2428SC <https://www.hirose.com/product/p/CL0550-0197-4-00>`__

   *  `PicoBlade Female-to-PicoBlade Female 151340400 <https://www.molex.com/molex/products/part-detail/cable_assemblies/0151340600>`__

      *  pin 1: VBAT-SENSE
      *  pin 2: ANT-SENSE
      *  pin 3: ANT-REL
      *  pin 4: COMMS-GND (GND that no connected to in base-plate GND)


   *  `JST SH Connector Female 2-Pin 1.0mm <http://grobotronics.com/images/datasheets/A1001H-XP-1---B5.pdf>`__

      *  pin 1: VBAT+ (TOP-PV+)
      *  pin 2: GND (TOP-PV-)
      *  `Connector assembly <http://www.jst-mfg.com/product/pdf/eng/handling_e.pdf>`__

      .. note:: we need to cut one side and we put `DF11-2428SCFA(04) <https://www.hirose.com/en/product/p/CL0543-0550-0-04>`__

.. list-table:: Harness Wiring
   :widths: 30 10 10 10 10 30
   :header-rows: 1

   * - Connector A
     - Pin A
     - Pin B
     - Connector B
     - Wire
     - Pin Name

   * - DF11-8DS-2C
     - 1
     - 3
     - DF11-6DS-2C
     - 26-AWG 80 mm (RD)
     - VBAT+

   * - DF11-8DS-2C
     - 2
     - 4
     - DF11-6DS-2C
     - 26-AWG 80 mm (YL)
     - EPS-PWR

   * - DF11-8DS-2C
     - 3
     - 1
     - DF11-6DS-2C
     - 26-AWG 80 mm (BK)
     - KILL-GND

   * - DF11-8DS-2C
     - 4
     - 2
     - DF11-6DS-2C
     - 26-AWG 80 mm (BL)
     - GND

   * - DF11-8DS-2C
     - 5
     - 4
     - PicoBlade 1.25mm 4-p
     - 28-AWG 50 mm (BK)
     - COMMS-GND

   * - DF11-8DS-2C
     - 6
     - 3
     - PicoBlade 1.25mm 4-p
     - 28-AWG 50 mm (BK)
     - ANT-REL

   * - DF11-8DS-2C
     - 7
     - 1
     - PicoBlade 1.25mm 4-p
     - 28-AWG 50 mm (BK)
     - VBAT-SENSE

   * - DF11-8DS-2C
     - 8
     - 2
     - PicoBlade 1.25mm 4-p
     - 28-AWG 50 mm (BK)
     - ANT-SENSE

   * - DF11-6DS-2C
     - 5
     - 2
     - JST SH F 1.0 mm 2-p
     - 28-AWG 50 mm (BK)
     - GND (TOP-PV-)

   * - DF11-6DS-2C
     - 6
     - 1
     - JST SH F 1.0 mm 2-p
     - 28-AWG 50 mm (BK)
     - VBAT+ (TOP-PV+)



.. figure:: img/main-cable.png
   :alt: main-cable

   Main Harness

COMMS Programmer cable
======================

*  Length: 50mm
*  1-pin is connected to 1-pin to other connector, so on one side we put new `1.25mm Pitch, PicoBlade Receptacle Crimp Housing <https://www.molex.com/molex/products/part-detail/crimp_housings/0510210600>`__.
*  `PicoBlade Female-to-PicoBlade Female 151340600 <https://www.molex.com/molex/products/part-detail/cable_assemblies/0151340600>`__

.. figure:: img/comms-jtag.png
   :alt: comms-jtag

   COMMS Programming Cable

Umbilical
=========

*  Length: In depends on the programmer.
*  pin1: VBAT-, black 26 AWG, when the switches is pushed, RBF in QUBIK
*  pin2: EPS-PWR, yellow 26 AWG, when the switches is pushed, RBF in QUBIK
*  pin3: GND, blue 26 AWG
*  pin4: VBAT+, red 26AWG
*  pin5: JTDO (COMMS), 28AWG red
*  pin6: NC
*  pin7: NRST (COMMS), 28AWG red
*  pin8: NC
*  pin9: SWDIO (COMMS), 28AWG red
*  pin10: NC
*  pin11: GND (COMMS), 28AWG black
*  pin12: NC
*  pin13: SWCLK (COMMS), 28AWG red
*  pin14: NC
*  pin15: VCCQ (COMMS), 28AWG red
*  pin16: NC
*  `Connector assembly <https://gr.mouser.com/datasheet/2/185/DF11_CL0543-0658-7-05_2d-1611074.pdf>`__

System Testing
**************
Related documentation:

* TBD
