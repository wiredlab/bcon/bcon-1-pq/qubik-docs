Structural
##########
* Description
* Repository
* Releases

System Perfomance
*****************
* Pass environmental tests.
* Document the CoM, https://gitlab.com/librespacefoundation/qubik/qubik-docs/-/issues/19

System Assembly
***************
Related issue:

* https://gitlab.com/librespacefoundation/qubik/qubik-docs/-/issues/10
* https://gitlab.com/librespacefoundation/qubik/qubik-docs/-/issues/15
* https://gitlab.com/librespacefoundation/qubik/qubik-docs/-/issues/11
* https://gitlab.com/librespacefoundation/qubik/qubik-docs/-/issues/14
* https://gitlab.com/librespacefoundation/qubik/qubik-docs/-/issues/20
* Before Gluing and coating a :doc:`dry fit <pre-assembly>` must be done

System Testing
**************
Related documentation:

* https://gitlab.com/librespacefoundation/qubik/qubik-structural/-/wikis/home
* Measure every mechanical part to be according to drawings
